package hw5pavlenko;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {

    public static final String WORD1 = "Hello World!";
    public static final String WORD2 = "I LOVE JAVA";
    public static final String WORD3 = "otpopto";

    @Test
    void findSymbolOccurance() {
        Service service = new Service();
        assertEquals(3,service.findSymbolOccurance(WORD1,'l'));
        assertEquals(0,service.findSymbolOccurance(WORD2,'a'));
        assertEquals(2,service.findSymbolOccurance(WORD3,'p'));
    }

    @Test
    void findWordPosition() {
        Service service = new Service();
        assertEquals(1,service.findWordPosition(WORD1,"World"));
        assertEquals(1,service.findWordPosition(WORD2,"VE"));
        assertEquals(-1,service.findWordPosition(WORD3,"pot"));
    }

    @Test
    void stringReverse() {
        Service service = new Service();
        assertEquals("!dlroW olleH",service.stringReverse(WORD1));
        assertEquals("AVAJ EVOL I",service.stringReverse(WORD2));
        assertEquals(WORD3,service.stringReverse(WORD3));
    }

    @Test
    void isPalindrome() {
        Service service = new Service();
        assertEquals(false,service.isPalindrome(WORD1));
        assertEquals(true,service.isPalindrome(WORD3));
    }
}