package hw5pavlenko;


import org.apache.commons.lang3.StringUtils;

import java.util.Random;

public class Service {

    public static final String START_MESSAGE = "Let`s start! Guess my word: ";
    public static final String ASK_USER_TO_GUESS_MESSAGE = "Type your guess: ";

    int findSymbolOccurance(String string, char c) {
        return StringUtils.countMatches(string, c);
    }

    int findWordPosition(String source, String target) {
        if (StringUtils.contains(source, target)) return 1;
        else return -1;
    }

    String stringReverse(String string) {
        return StringUtils.reverse(string);
    }

    boolean isPalindrome(String string) {
        return StringUtils.reverse(string).equals(string);
    }

    public void runGame(String[] words, Ui ui) {
        ui.message(START_MESSAGE);
        boolean repeatSwitch = true;
        String chosenWord = chooseRandomWord(words);
        do {
            ui.message(ASK_USER_TO_GUESS_MESSAGE);
            String usersGuess = ui.readUsersInput();
            if (usersGuess.equals(chosenWord)) {
                ui.WinMessage(chosenWord);
                repeatSwitch = false;
            } else {
                ui.message(getGuessedCharacters(chosenWord, usersGuess));
            }
        } while (repeatSwitch);
    }

    private String getGuessedCharacters(String chosenWord, String usersGuess) {
        StringBuilder quizzedWord = new StringBuilder("###############");
        int shortestWordLenght = Math.min(chosenWord.length(), usersGuess.length());
        for (int i = 0; i < shortestWordLenght; i++) {
            if (usersGuess.charAt(i) == chosenWord.charAt(i)) {
                quizzedWord.setCharAt(i, usersGuess.charAt(i));
            }
        }
        return String.valueOf(quizzedWord);
    }

    private String chooseRandomWord(String[] words) {
        Random random = new Random();
        int randomIndex = random.nextInt(words.length);
        return words[randomIndex];
    }
}
