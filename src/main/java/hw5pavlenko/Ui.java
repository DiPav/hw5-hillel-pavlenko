package hw5pavlenko;

import java.util.Scanner;

public class Ui {
    private final Scanner scanner;

    public Ui(Scanner scanner) {
        this.scanner = scanner;
    }

    public void message(String message) {
        System.out.println(message);
    }

    public String readUsersInput() {
        return scanner.nextLine().toLowerCase().replaceAll(" ", "");
    }

    public void WinMessage(String chosenWord) {
        System.out.printf("\nYou win! My word was %s !\n",chosenWord);
    }

}
